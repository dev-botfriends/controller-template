const { FB_PAGE_TOKEN, FB_VERIFY_TOKEN, FB_APP_SECRET, GOOGLE_PROJECT_ID, GOOGLE_CLIENT_EMAIL, GOOGLE_PRIVATE_KEY, DF_LANGUAGE_CODE } = require('../config')

const checkEnvVariables = () => {
  if (!FB_PAGE_TOKEN) {
    throw new Error('missing FB_PAGE_TOKEN')
  }
  if (!FB_VERIFY_TOKEN) {
    throw new Error('missing FB_VERIFY_TOKEN')
  }
  if (!FB_APP_SECRET) {
    throw new Error('missing FB_APP_SECRET')
  }
  if (!GOOGLE_PROJECT_ID) {
    throw new Error('missing GOOGLE_PROJECT_ID')
  }
  if (!GOOGLE_CLIENT_EMAIL) {
    throw new Error('missing GOOGLE_CLIENT_EMAIL')
  }
  if (!GOOGLE_PRIVATE_KEY) {
    throw new Error('missing GOOGLE_PRIVATE_KEY')
  }
  if (!DF_LANGUAGE_CODE) {
    throw new Error('missing DF_LANGUAGE_CODE')
  }
}

module.exports = {
  checkEnvVariables
}
