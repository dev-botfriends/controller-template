const express = require('express')
const router = express.Router()
const { FB_VERIFY_TOKEN } = require('../config')
const { receivedEvent } = require('../services/messageReceiver')

router.get('/webhook/', (req, res) => {
  if (req.query['hub.mode'] === 'subscribe' && req.query['hub.verify_token'] === FB_VERIFY_TOKEN) {
    res.status(200).send(req.query['hub.challenge'])
  } else {
    console.error('Failed validation. Make sure the validation tokens match.')
    res.sendStatus(403)
  }
})

router.post('/webhook/', (req, res) => {
  const { object, entry } = req.body
  if (object === 'page') {
    entry.forEach(pageEntry => {
      pageEntry.messaging.forEach(messagingEvent => {
        receivedEvent(messagingEvent)
      })
    })
    res.sendStatus(200)
  }
})

module.exports = router
