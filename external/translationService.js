const axios = require('axios')
const { TRANSLATION_API_KEY } = require('../config')

const baseUrl = 'https://translate.yandex.net/api/v1.5/tr.json'

const translate = async (sentence, language) => {
  try {
    const url = `${baseUrl}/translate?key=${TRANSLATION_API_KEY}&text=${sentence}&lang=en-${getLanguageCode(language)}`
    const response = await axios.get(url)
    return response.data ? response.data.text[0] : ''
  } catch(err) {
    console.log(err)
    return ''
  }
}

const detectLanguage = async (sentence) => {
  try {
    const url = `${baseUrl}/detect?key=${TRANSLATION_API_KEY}&text=${sentence}`
    const response = await axios.get(url)
    return response.data ? response.data.lang : ''
  } catch(err) {
    console.log(err)
    return ''
  }
}

const getLanguageCode = (language) => {
  switch (language.toLowerCase()) {
    case 'german':
    case 'germany':
      return 'de'
    case 'dutch':
    case 'netherlands':
      return 'nl'
    case 'polish':
    case 'poland':
      return 'pl'
    case 'russian':
    case 'russia':
      return 'ru'
    case 'spanish':
    case 'spain':
      return 'es'
  }
}

module.exports = {
  translate,
  detectLanguage
}
