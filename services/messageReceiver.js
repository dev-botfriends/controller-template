const { sendToDialogFlow } = require('./dialogFlowService')

const receivedEvent = event => {
  const senderID = event.sender.id
  if (event.message) {
    const message = event.message
    const { text, quick_reply: quickReply } = message
    sendToDialogFlow(senderID, quickReply ? quickReply.payload : text)
  } else if (event.postback) {
    const payload = event.postback.payload
    switch (payload) {
      default:
        sendToDialogFlow(senderID, payload)
        break
    }
  }
}

module.exports = {
  receivedEvent
}
