const dialogflow = require('dialogflow')
const { GOOGLE_PROJECT_ID, GOOGLE_CLIENT_EMAIL, GOOGLE_PRIVATE_KEY, DF_LANGUAGE_CODE } = require('../config')
const { sendTyping, sendTextMessage } = require('./facebookService')
const { getTemperatureByCityName } = require('../external/weatherService')
const { translate } = require('../external/translationService')
const { getCryptoTicker } = require('../external/cryptoCurrencyService')
const { registerUser, showName } = require('../internal/userService')
const { handleMessages } = require('./messageHandler')
const { isDefined } = require('../utils/helper')

const credentials = {
  projectId: GOOGLE_PROJECT_ID,
  credentials: {
    client_email: GOOGLE_CLIENT_EMAIL,
    private_key: GOOGLE_PRIVATE_KEY
  }
}

const sendToDialogFlow = async (sender, textString, params) => {
  sendTyping(sender, 'on')
  try {
    const sessionClient = new dialogflow.SessionsClient(credentials)
    const sessionPath = sessionClient.sessionPath(GOOGLE_PROJECT_ID, sender)
    const request = {
      session: sessionPath,
      queryInput: {
        text: {
          text: textString,
          languageCode: DF_LANGUAGE_CODE
        }
      },
      queryParams: {
        payload: {
          data: params
        }
      }
    }
    const responses = await sessionClient.detectIntent(request)
    const response = responses[0].queryResult
    await handleDialogFlowResponse(sender, response)
  } catch (e) {
    console.log(e)
  }
}

const handleDialogFlowResponse = async (sender, response) => {
  const { fulfillmentMessages, action, outputContexts, parameters } = response
  const { fulfillmentText } = fulfillmentMessages
  await sendTyping(sender, 'off')
  if (isDefined(action)) {
    await handleDialogFlowAction(sender, action, fulfillmentMessages, outputContexts, parameters)
  } else if (isDefined(fulfillmentMessages)) {
    await handleMessages(fulfillmentMessages, sender)
  } else if (fulfillmentText === '' && !isDefined(action)) {
    await sendTextMessage(sender, "I'm not sure what you want. Can you be more specific?")
  } else if (isDefined(fulfillmentText)) {
    await sendTextMessage(sender, fulfillmentText)
  }
}

const handleDialogFlowAction = async (sender, action, messages, contexts, parameters) => {
  switch (action) {
    case 'weather':
      const city = parameters.fields['geo-city'].stringValue
      if (city) {
        const temperature = await getTemperatureByCityName(city)
        if (temperature) {
          messages = [buildTextMessage(messages[0].text.text[0].replace('$temperature', temperature))]
        }
      }
      break
    case 'translation': 
      const language = parameters.fields.language.stringValue
      const sentence = parameters.fields.sentence.stringValue
      if (language && sentence) {
        const text = await translate(sentence, language)
        if (text) {
          messages = [buildTextMessage(`The ${language.toLowerCase()} translation for "${sentence}" is "${text}".`)]
        }
      }
      break
    case 'cryptocurrency': 
      const coinsTicker = await getCryptoTicker()
      if (coinsTicker) {
        messages = []
        coinsTicker.forEach(coin => {
          const message = buildTextMessage(`Price ${coin.currency}: ${coin.price}$`)
          messages.push(message)
        })
      }
      break
    case 'register':
      const name = parameters.fields.name.stringValue
      if (name) {
        const text = await registerUser(sender, name)
        if (text) {
          messages = [buildTextMessage(text)]
        }
      }
      break
    case 'showname':
      const text = await showName(sender)
      if (text) {
        messages = [buildTextMessage(text)]
      }
      break
    default:
      break
  }
  await handleMessages(messages, sender)
}

const buildTextMessage = (message) => {
  return {
    platform: 'PLATFORM_UNSPECIFIED',
      text: { text: [message] },
      message: 'text'
  }
}

module.exports = {
  sendToDialogFlow
}
