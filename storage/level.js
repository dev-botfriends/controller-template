const level = require('level')

const db = level('data')

const levelInsert = async (key, value) => {
  try {
    await db.put(key, value)
    return {
      success: true
    }
  } catch (err) {
    console.log(err)
    return {
      success: false,
      message: err.message
    }
  }
}

const levelSelect = async (key) => {
  try {
    const value = await db.get(key)
    return {
      success: true,
      value
    }
  } catch (err) {
    console.log(err)
    return {
      success: false,
      message: err.message
    }
  }
}

const levelDelete = async (key) => {
  try {
    await db.del(key)
    return {
      success: true
    }
  } catch (err) {
    console.log(err)
    return {
      success: false,
      message: err.message
    }
  }
}

module.exports = {
  levelInsert,
  levelSelect,
  levelDelete
}
